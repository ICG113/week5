
import random

primeLoop = True

def is_prime(num):
    for x in range (2,num):
        if (num%x == 0):
            return False
    return True

while primeLoop:
    val = random.randint(1, 100)
    if (is_prime(val)):
        print(val, "is prime.")
    else:
        print(val, "is not prime.")
    ask = input("Would you like to continue? Y/N: ")
    if ask == "N" or ask == "n":
        primeLoop = False
        print("Program terminated.")

        










