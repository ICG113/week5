
def is_prime(num):
    for x in range (2,num):
        if (num%x == 0):
            return False
    return True

for val in range (1,101):
    if (is_prime(val)):
        print(val, "is prime.")
    else:
        print(val, "is not prime.")
    
